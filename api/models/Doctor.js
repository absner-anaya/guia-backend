/**
 * Doctor.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,
  attributes: {
    cedula: {
      type: 'string',
      required: true,
      unique: true
    },
    rif: {
      type: 'string',
      unique: true
    },
    first_name: {
      type: 'string'
    },
    last_name: {
      type: 'string'
    },
    phone: {
      type: 'string'
    },
    email: {
      type: 'email',
      unique: true
    },
    description: {
      type: 'text'
    },
    prefi: {
      type: 'string'
    }
  }
};

