/**
 * EspecialidadController
 *
 * @description :: Server-side logic for managing especialidads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    create: function (req, res){
        if (Object.keys(req.body).length === 0){
            res.json({
                code: 200,
                message: 'No hay data'
            })
        }else{
            Especialidad.create(req.body
            ).exec(function (err, data) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'success',
                    content: data
                });
            })
        }

    },
    find: function (req, res) {
        
        Especialidad.find().exec(function (err, data) {
            if(err) return res.json({ err: err }, 500);
            else res.json(data);
        });
    },
    findOne: function (req, res) {

        if (req.params.id === undefined ){
            res.json({
                code: 200,
                message: 'no ah proporcionado un id'
            })
        }else{
            Especialidad.findOne({ id: req.params.id }).exec(function (err, data) {
              if(err) return res.json({ err: err }, 500);
              else res.json(data);
            });
        }
    },
    destroy: function (req, res) {
        if (req.params.id === undefined ){
            res.json({
                code: 200,
                message: 'no ah proporcionado un id'
            })
        }else{
            Especialidad.destroy({id: req.params.id}).exec(function (err, data) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'user delete',
                    content: data
                });
            });
        }
    },
    update: function (req, res) {
        if (req.params.id === undefined ){
            res.json({
                code: 200,
                message: 'no ah proporcionado un id'
            })
        }else{
            Especialidad.update(
                {id: req.params.id},
                req.body
            ).exec(function (err, data) {
                if(err){ 
                    return res.json({ err: err }, 500);
                }else { 

                    return res.json({
                        code: 200,
                        message: 'Update data',
                        content: data[0]
                    });
                }
            });
        }
    }
	
};

